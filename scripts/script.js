$(document).ready(function(){    
    /*
    | СБРОС НАСТРОЕК
    */ 
    $("#header").click(function(){ DiscardEdits($(this)); });
    
    /*
    | ОСНОВНЫЕ СОБЫТИЯ
    */   
    $("#items-count").on("keypress", function(obj){ InputValidate(obj); });
    $("#items-create").click(function(){ ItemsCreateClick($(this)); });
    $("#collapse-edit-body").click(function(){
        $(".edit-body").slideToggle(75, () => {
            if ($(".edit-body").css("display") === "none"){
                $("html, body").css({"minHeight": "400px"});
            } else{ 
                $("html, body").css({"minHeight": "700px"});
            }
            
            $(".edit-body-gear").fadeToggle(100);
            //$(".edit-body-gear").toggle();
        });
    });
    
    /*
    | СОБЫТИЯ ИЗМЕНЕНИЯ ПАРАМЕТРОВ FLEX-BOX КОНТЕЙНЕРА
    */  
    $("#flex-dir-row, #flex-dir-col").each(function(){
        $(this).click(function(){
            $(".main-blocks").css({"flex-direction": $(this).val()});
        });    
    });     
    $("#flex-wrap-wrap, #flex-wrap-nowrap").each(function(){
        $(this).click(function(){
            $(".main-blocks").css({"flex-wrap": $(this).val()});
        });    
    });     
    $("#select-jc").change(function(){
        $(".main-blocks").css({"justify-content": $(this).val()});  
    });     
    $("#select-ai").change(function(){
        $(".main-blocks").css({"align-items": $(this).val()});  
    });     
    $("#select-ac").change(function(){
        $(".main-blocks").css({"align-content": $(this).val()});  
    });
    
    /*
    | УПРАВЛЕНИЕ УНИКЛЬНЫМ БЛОКОМ
    */ 
    $(".block-display").click(function(){ BlockDisplayClick($(this)); });
    $("#item-order, #item-grow, #item-shrink").each(function(){
        $(this).on("keypress", function(e){ InputPropValidate(e); })
    });
    $("#item-order").on("keyup", () => { block.id.css({"order": $("#item-order").val()}); });
    $("#select-as").change(function(){ block.id.css({"align-self": $(this).val()}); }); 
    $("#item-grow").on("keyup", () => { block.id.css({"flex-grow": $("#item-grow").val()}); });
    $("#item-shrink").on("keyup", () => { block.id.css({"flex-shrink": $("#item-shrink").val()}); });
    $("#item-basis").on("keyup", () => { block.id.css({"flex-basis": $("#item-basis").val()}); });
    
    /*
    | ИНИЦИАЛИЗАЦИЯ ВЫБОРА ПЕРВОГО БЛОКА
    */ 
    $(".block-display").first().click(); 
});

var block = {"id": null, "text": ""};

function DiscardEdits(ts){
    $("#flex-dir-row").click();
    $("#flex-wrap-wrap").click();
    $("#select-jc").val("flex-start");
    $("#select-ai").val("flex-start");
    $("#select-ac").val("stretch");
    $("#item-order").val("0");
    $("#select-as").val("auto");
    $("#item-grow").val("0");
    $("#item-shrink").val("1");
    $("#item-basis").val("auto");
    
    $(".main-blocks").css({"justify-content": "flex-start"});
    $(".main-blocks").css({"align-items": "flex-start"});
    $(".main-blocks").css({"align-content": "stretch"});
    
    $(".block-display").each(function(){
        $(this).css({"order": "0"});
        $(this).css({"align-self": "auto"});
        $(this).css({"flex-grow": "0"});
        $(this).css({"flex-shrink": "1"});
        $(this).css({"flex-basis": "auto"});
    });
    
    $("#flex-dir-row").focus().blur();
}

function InputValidate(ts){    
    if (ts.keyCode == 13){
        console.log("enter");
        $("#items-create").click();
        ts.preventDefault();
        return;
    } 
    
    if (ts.keyCode < 48 || ts.keyCode > 57){
        ts.preventDefault();
        ts.returnValue = false;
    }
}
function InputPropValidate(ts){
    if (block.id == null){
        ts.preventDefault();
        return;
    }
    
    if (ts.keyCode < 48 || ts.keyCode > 57){
        ts.preventDefault();
        ts.returnValue = false;
    }
}

function ItemsCreateClick(ts){
    let txtVal = Number($("#items-count").val());
    
    if (txtVal == 0) {
        $(".block-display").each(function(){
            $(this).css({"height": randomHeight() + "px"})
        });
        return;
    }
    if (txtVal < 1 || txtVal > 20 || Number.isNaN(txtVal)){
        alert("Допустимый целочисленный диапазон значений от 1 до 20!");
        return;
    } 
    
    if (txtVal < $(".block-display").length){
        while ($(".block-display").length != txtVal)
            $(".main-blocks").children().last().remove();
        
        $(".block-display").last().click(); 
    }
    else if (txtVal > $(".block-display").length){
        while ($(".block-display").length != txtVal)
            $(".block-display")
                .first()
                .clone()
                .html("<span>#" + ($(".block-display").length + 1) + "</span>")
                .appendTo(".main-blocks");            
    }    
    
    $(".block-display").each(function(){
        $(this).unbind();
        $(this).click(function(){ BlockDisplayClick($(this)); });
        $(this).css({"height": randomHeight() + "px"})
    });
    
    $("#items-count").val("");
    $(".block-display").first().click(); 
}
function BlockDisplayClick(ts){    
    let val = ts.children("span").text().replace("#", "");
    
    $("#selected-block").text(val);
    $("#item-order").val(ts.css("order"));
    $("#select-as").val(ts.css("align-self"));
    $("#item-grow").val(ts.css("flex-grow"));
    $("#item-shrink").val(ts.css("flex-shrink"));
    $("#item-basis").val(ts.css("flex-basis"));
    
    block.id = ts;
    block.text = val;
    
    $(".block-display").css({"backgroundColor": "white"});
    ts.css({"backgroundColor": "rgba(0, 255, 255, .25)"});
    
    //background-color: #cde7ff;
}

function randomHeight(){
    let rnd = Math.floor((Math.random() * 35));
    let flag = Math.random() > 0.5 ? true : false;
               
    return flag ? 60 + rnd : 60 - rnd;
}









